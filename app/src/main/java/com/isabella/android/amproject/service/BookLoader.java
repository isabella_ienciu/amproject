package com.isabella.android.amproject.service;

import android.content.Context;
import android.util.Log;

import com.isabella.android.amproject.content.Book;
import com.isabella.android.amproject.net.BookRestClient;
import com.isabella.android.amproject.util.OkAsyncTaskLoader;

import java.util.List;

/**
 * Created by Isabella on 11/6/2016.
 */

public class BookLoader extends OkAsyncTaskLoader<List<Book>> {
    private static final String TAG = BookLoader.class.getSimpleName();
    private final BookRestClient mBookRestClient;
    private List<Book> books;

    public BookLoader(Context context, BookRestClient bookRestClient) {
        super(context);
        mBookRestClient = bookRestClient;
    }

    @Override
    public List<Book> tryLoadInBackground() throws Exception {
        Log.d(TAG, "tryLoadInBackground");
        books = mBookRestClient.getAll();
        return books;
    }

    @Override
    protected void onStartLoading() {
        if (books != null) {
            Log.d(TAG, "onStartLoading - deliver result");
            deliverResult(books);
        }

        if (takeContentChanged() || books == null) {
            Log.d(TAG, "onStartLoading - force load");
            forceLoad();
        }
    }
}