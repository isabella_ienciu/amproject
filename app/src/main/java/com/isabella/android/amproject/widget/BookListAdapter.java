package com.isabella.android.amproject.widget;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.isabella.android.amproject.service.BookManager;
import com.isabella.android.amproject.R;
import com.isabella.android.amproject.content.Book;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Isabella on 10/19/2016.
 */

public class BookListAdapter extends BaseAdapter {
    public static final String TAG = BookListAdapter.class.getSimpleName();
    private final ArrayList<Book> mBooks;
    private final Context mContext;

    public BookListAdapter(Context context, ArrayList<Book> books) {
        mContext = context;
        mBooks = books;
    }

    @Override
    public int getCount() {
        return mBooks.size();
    }

    @Override
    public Object getItem(int position) {
        return mBooks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View bookLayout = LayoutInflater.from(mContext).inflate(R.layout.book_detail, null);
        ((TextView)bookLayout.findViewById(R.id.book_title)).setText(mBooks.get(position).getTitle());
        Log.d(TAG, "getView " + position);
        return bookLayout;
    }
}
