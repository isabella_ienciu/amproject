package com.isabella.android.amproject.service;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.isabella.android.amproject.content.Book;
import com.isabella.android.amproject.content.LibraryDatabase;
import com.isabella.android.amproject.content.User;
import com.isabella.android.amproject.net.BookRestClient;
import com.isabella.android.amproject.net.LastModifiedList;
import com.isabella.android.amproject.util.Cancellable;
import com.isabella.android.amproject.util.OkCancellableCall;
import com.isabella.android.amproject.util.OnErrorListener;
import com.isabella.android.amproject.util.OnSuccessListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by Isabella on 10/19/2016.
 */

public class BookManager {
    private static final String TAG = BookManager.class.getSimpleName();

    private List<Book> mBooks;
    private ConcurrentMap<String, Book> myBooks = new ConcurrentHashMap<String, Book>();
    private OnNoteUpdateListener mOnUpdate;

    private final Context mContext;
    private BookRestClient mBookRestClient;
    private final LibraryDatabase mLDB;

    private User currentUser;
    private String mToken;

    private String mBooksLastUpdate;

    public BookManager(Context context) {
        mContext = context;
        currentUser = null;
        mLDB = new LibraryDatabase(context);
    }

    public BookLoader getBookLoader() {
        Log.d(TAG, "getBookLoader...");
        return new BookLoader(mContext, mBookRestClient);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public Cancellable addBook(final String title, final OnSuccessListener<String> successListener, final OnErrorListener errorListener) throws Exception {
        return mBookRestClient.addBook(
                title, new OnSuccessListener<String>() {

                    @Override
                    public void onSuccess(String message) {
                        if (message != null) {
                            successListener.onSuccess(message);
                        } else {
                            errorListener.onError(new IllegalArgumentException("Book not added"));
                        }
                    }
                }, errorListener);
    }

    public void setBookRestClient(BookRestClient bookRestClient) {
        mBookRestClient = bookRestClient;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public Cancellable getBooksAsync(final OnSuccessListener<List<Book>> successListener, OnErrorListener errorListener) {
        Log.d(TAG, "getNotesAsync...");
        return mBookRestClient.searchAsync(mBooksLastUpdate, new OnSuccessListener<LastModifiedList<Book>>() {

            @Override
            public void onSuccess(LastModifiedList<Book> result) {
                Log.d(TAG, "getNotesAsync succeeded");
                List<Book> books = result.getList();
                if (books != null) {
                    mBooksLastUpdate = result.getLastModified();
                    updateCachedBooks(books);
                }
                successListener.onSuccess(cachedBooksByUpdated());
            }
        }, errorListener);
    }

    private void updateCachedBooks(List<Book> books) {
        Log.d(TAG, "updateCachedNotes");
        for (Book book : books) {
            myBooks.put(book.getTitle(), book);
        }
    }

    private List<Book> cachedBooksByUpdated() {
        ArrayList<Book> books = new ArrayList<>(myBooks.values());
        return books;
    }

    public void deleteBook(String title) {
        for(int i=0;i<mBooks.size();i++){
            if(mBooks.get(i).getTitle().equals(title)){
                mBooks.remove(i);
            }
        }
    }

    public void editBook(String titles) {
        List<String> list = new ArrayList<String>(Arrays.asList(titles.split(" , ")));
        for(int i=0;i<mBooks.size();i++){
            if(mBooks.get(i).getTitle().equals(list.get(0))){
                mBooks.get(i).setTitle(list.get(1));
            }
        }
    }

    public interface OnNoteUpdateListener {
        void updated();
    }

    public Cancellable loginAsync(String username, String password, final OnSuccessListener<String> successListener, final OnErrorListener errorListener) throws Exception {
        final User user = new User(username, password);
        return mBookRestClient.getToken(
                user, new OnSuccessListener<String>() {

                    @Override
                    public void onSuccess(String token) {
                        mToken = token;
                        if (mToken != null) {
                            user.setToken(mToken);
                            setCurrentUser(user);
                            mLDB.saveUser(user);
                            successListener.onSuccess(mToken);
                        } else {
                            errorListener.onError(new IllegalArgumentException("Invalid credentials"));
                        }
                    }
                }, errorListener);
    }

    public void setCurrentUser(User user){
        this.currentUser=user;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public User getCurrentUser(){
        return mLDB.getCurrentUser();
    }
}
