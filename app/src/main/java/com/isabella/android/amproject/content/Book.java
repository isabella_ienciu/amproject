package com.isabella.android.amproject.content;

import java.util.Date;

/**
 * Created by Isabella on 10/19/2016.
 */

public class Book {
    private String bID;
    private String title;
    private String author;
    private Date published;

    public Book(String bID, String title, String author, Date published){
        this.bID=bID;
        this.title=title;
        this.author=author;
        this.published=published;
    }

    public Book() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getPublished() {
        return published;
    }

    public void setPublished(Date published) {
        this.published = published;
    }

    public String getbID() {
        return bID;
    }

    public void setbID(String bID) {
        this.bID = bID;
    }
}
