package com.isabella.android.amproject.net.mapping;

import android.util.JsonReader;

import com.isabella.android.amproject.util.ResourceReader;
import com.isabella.android.amproject.util.ResourceReader2;

import java.io.IOException;


public class TokenReader implements ResourceReader2<String, JsonReader> {
  @Override
  public String read(JsonReader reader) throws IOException {
    reader.beginObject();
    String token = null;
    while (reader.hasNext()) {
      String name = reader.nextName();
      if (name.equals("token")) {
        token = reader.nextString();
      }
    }
    reader.endObject();
    return token;
  }
}
