package com.isabella.android.amproject.util;

import android.util.JsonWriter;

import java.io.IOException;

public interface ResourceWriter2<E, Writer> {
    void write(E e, Writer writer) throws IOException;
}
