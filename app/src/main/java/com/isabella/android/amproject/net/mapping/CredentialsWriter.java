package com.isabella.android.amproject.net.mapping;

import android.util.JsonWriter;

import com.isabella.android.amproject.content.User;
import com.isabella.android.amproject.util.ResourceWriter;
import com.isabella.android.amproject.util.ResourceWriter2;

import java.io.IOException;

public class CredentialsWriter implements ResourceWriter2<User, JsonWriter> {
  @Override
  public void write(User user, JsonWriter writer) throws IOException {
    writer.beginObject();
    {
      writer.name("username").value(user.getUsername());
      writer.name("password").value(user.getPassword());
    }
    writer.endObject();
  }
}