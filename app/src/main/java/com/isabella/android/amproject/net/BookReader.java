package com.isabella.android.amproject.net;

import android.provider.ContactsContract;
import android.util.JsonReader;
import android.util.Log;

import com.isabella.android.amproject.content.Book;
import com.isabella.android.amproject.util.ResourceReader;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Isabella on 11/6/2016.
 */

public class BookReader implements ResourceReader<Book> {
    private static final String TAG = BookReader.class.getSimpleName();
    public static final String BOOK_ID = "_id";
    public static final String BOOK_TITLE = "title";
    public static final String BOOK_AUTHOR = "author";
    public static final String BOOK_PUBLISHED = "published";

    @Override
    public Book read(JsonReader reader) throws IOException {
        Book book = new Book();
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals(BOOK_ID)) {
                book.setbID(reader.nextString());
            } else if (name.equals(BOOK_TITLE)) {
                book.setTitle(reader.nextString());
            } else if (name.equals(BOOK_AUTHOR)) {
                book.setAuthor(reader.nextString());
            } else if (name.equals(BOOK_PUBLISHED)) {
                Date d = new Date();
                try {
                    d = df.parse(reader.nextString());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                book.setPublished(d);
            } else {
                reader.skipValue();
                Log.w(TAG, String.format("Book property '%s' ignored", name));
            }
        }
        reader.endObject();
        return book;
    }
}
