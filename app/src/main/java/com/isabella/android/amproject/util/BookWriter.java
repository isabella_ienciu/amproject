package com.isabella.android.amproject.util;

import android.util.JsonWriter;

import com.isabella.android.amproject.content.User;

import java.io.IOException;

/**
 * Created by Isabella on 1/18/2017.
 */

public class BookWriter implements ResourceWriter2<String, JsonWriter> {
    @Override
    public void write(String title, JsonWriter writer) throws IOException {
        writer.beginObject();
        {
            writer.name("title").value(title);
            writer.name("author").value(title+" author");
        }
        writer.endObject();
    }
}