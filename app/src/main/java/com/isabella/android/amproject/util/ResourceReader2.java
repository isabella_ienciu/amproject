package com.isabella.android.amproject.util;

import org.json.JSONException;

import java.io.IOException;

/**
 * Created by Isabella on 1/17/2017.
 */

public interface ResourceReader2<E, Reader> {
    E read(Reader reader) throws IOException, JSONException, Exception;
}
