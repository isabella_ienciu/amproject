package com.isabella.android.amproject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
import com.isabella.android.amproject.LibraryApplication;
import com.isabella.android.amproject.R;
import com.isabella.android.amproject.service.BookManager;
import com.isabella.android.amproject.util.Cancellable;
import com.isabella.android.amproject.util.OnErrorListener;
import com.isabella.android.amproject.util.OnSuccessListener;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {
    public static final String TAG = LoginActivity.class.getSimpleName();

    // UI references.
    private EditText mEmailView;
    private EditText mPasswordView;
    private Button mLogInButton;

    private BookManager mBookManager;
    private Cancellable mCancellable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mEmailView = (EditText) findViewById(R.id.email);
        mPasswordView = (EditText) findViewById(R.id.password);
        mLogInButton = (Button) findViewById(R.id.email_sign_in_button);

        LibraryApplication mApp = (LibraryApplication) getApplication();
        mBookManager = mApp.getmBookManager();

        setUpLogin();

        Log.d(TAG, "onCreate");
    }

    private void setUpLogin() {

        mLogInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = mEmailView.getText().toString();
                String password = mPasswordView.getText().toString();

                try {
                    mCancellable = mBookManager
                            .loginAsync(
                                    username, password,
                                    new OnSuccessListener<String>() {
                                        @Override
                                        public void onSuccess(String s) {
                                            runOnUiThread(new Runnable() {
                                                @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                                                @Override
                                                public void run() {

                                                    Intent intent = new Intent("com.rj.notitfications.SECACTIVITY");

                                                    PendingIntent pendingIntent = PendingIntent.getActivity(LoginActivity.this, 1, intent, 0);

                                                    Notification.Builder builder = new Notification.Builder(LoginActivity.this);

                                                    builder.setAutoCancel(false);
                                                    builder.setContentTitle("Library App Notification");
                                                    builder.setContentText("You are now logged in!");
                                                    builder.setSmallIcon(R.drawable.noti_icon);
                                                    builder.setContentIntent(pendingIntent);
                                                    builder.setOngoing(true);
                                                    builder.build();

                                                    Notification myNotication = builder.getNotification();
                                                    NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);;
                                                    manager.notify(11, myNotication);

                                                    Intent k = new Intent(LoginActivity.this, BookListActivity.class);
                                                    startActivity(k);
                                                }
                                            });
                                        }
                                    }, new OnErrorListener() {
                                        @Override
                                        public void onError(final Exception e) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(getApplicationContext(), "Wrong Credentials", Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }
                                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

    }



    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }
}


