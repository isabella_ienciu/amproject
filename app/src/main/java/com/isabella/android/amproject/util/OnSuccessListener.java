package com.isabella.android.amproject.util;

public interface OnSuccessListener<E> {
    void onSuccess(E e);
}
