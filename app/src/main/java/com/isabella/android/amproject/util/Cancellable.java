package com.isabella.android.amproject.util;

public interface Cancellable {
  void cancel();
}
