package com.isabella.android.amproject.util;

public interface OnErrorListener {
    void onError(Exception e);
}
