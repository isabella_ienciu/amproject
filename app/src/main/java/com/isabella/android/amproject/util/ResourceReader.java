package com.isabella.android.amproject.util;

import android.util.JsonReader;

import java.io.IOException;

/**
 * Created by Isabella on 11/6/2016.
 */

public interface ResourceReader<E> {
    E read(JsonReader reader) throws IOException;
}
