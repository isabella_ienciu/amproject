package com.isabella.android.amproject;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.isabella.android.amproject.content.Book;
import com.isabella.android.amproject.service.BookManager;
import com.isabella.android.amproject.util.Cancellable;
import com.isabella.android.amproject.util.OkCancellableCall;
import com.isabella.android.amproject.util.OnErrorListener;
import com.isabella.android.amproject.util.OnSuccessListener;
import com.isabella.android.amproject.widget.BookListAdapter;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class BookListActivity extends AppCompatActivity {
    public static final String TAG = BookListActivity.class.getSimpleName();

    private LibraryApplication mApp;
    private BookManager mBookManager;

    private Cancellable mGetBooksAsyncCall;
    private Cancellable mAddBookAsyncCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApp = (LibraryApplication) getApplication();
        mBookManager = mApp.getmBookManager();
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        addButtons();

        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(5000);
                        runOnUiThread(new Runnable() {
                            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                            @Override
                            public void run() {
                                startGetBooksAsyncCall();
                                Toast.makeText(getApplicationContext(),
                                        "List updated",Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        t.start();

        Log.d(TAG, "onCreate");
    }
    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d(TAG, "onRestoreInstanceState");
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart activity");
        startGetBooksAsyncCall();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void startGetBooksAsyncCall() {

        final BookListFragment bookListFragment = (BookListFragment) getSupportFragmentManager().findFragmentById(R.id.bookFragment);

        mGetBooksAsyncCall = mApp.getmBookManager().getBooksAsync(
                new OnSuccessListener<List<Book>>() {
                    @Override
                    public void onSuccess(final List<Book> books) {
                        Log.d(TAG, "getNotesAsyncCall - success");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                bookListFragment.showContent(books);
                            }
                        });
                    }
                }, new OnErrorListener() {
                    @Override
                    public void onError(final Exception e) {
                        Log.d(TAG, "getNotesAsyncCall - error");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                bookListFragment.showError(e);
                            }
                        });
                    }
                }
        );
    }


    private void addButtons(){
        final EditText editTitle = (EditText) findViewById(R.id.edit_title);

        Button add = (Button) findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View view) {
                String title = editTitle.getText().toString();
                try {
                    mAddBookAsyncCall = mBookManager
                            .addBook(
                                    title,
                                    new OnSuccessListener<String>() {
                                        @Override
                                        public void onSuccess(final String s) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(getApplicationContext(),
                                                            s,Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }
                                    }, new OnErrorListener() {
                                        @Override
                                        public void onError(final Exception e) {
                                            runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    Toast.makeText(getApplicationContext(), "Book not added",Toast.LENGTH_SHORT).show();
                                                }
                                            });
                                        }
                                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }
}