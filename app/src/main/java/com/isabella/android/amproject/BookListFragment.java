package com.isabella.android.amproject;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.isabella.android.amproject.content.Book;
import com.isabella.android.amproject.util.OkAsyncTask;
import com.isabella.android.amproject.util.OkAsyncTaskLoader;
import com.isabella.android.amproject.util.OkCancellableCall;
import com.isabella.android.amproject.util.OnErrorListener;
import com.isabella.android.amproject.util.OnSuccessListener;
import com.isabella.android.amproject.widget.BookListAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Isabella on 11/7/2016.
 */

public class BookListFragment extends Fragment implements LoaderManager.LoaderCallbacks<List<Book>> {

    public static final String TAG = BookListFragment.class.getSimpleName();
    private LibraryApplication mApp;
    private BookListAdapter mBookListAdapter;
    private ListView mBookListView;
    private View mContentLoadingView;
    private OkCancellableCall mGetBooksAsyncCall;


    @Override
    public void onAttach(Context context) {
        Log.d(TAG, "onAttach");
        super.onAttach(context);
        mApp = (LibraryApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View layout = inflater.inflate(R.layout.fragment_books, container, false);
        mBookListView = (ListView) layout.findViewById(R.id.book_list);
        mContentLoadingView = layout.findViewById(R.id.content_loading);
        showLoadingIndicator();
        return layout;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onViewCreated");
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart fragment");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.d(TAG, "onResume");
        super.onResume();
    }

    @Override
    public void onPause() {
        Log.d(TAG, "onPause");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop");
        super.onStop();
        ensureGetBooksAsyncCallCancelled();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "onSaveInstanceState");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "onDetach");
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "onDestroyView");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }

    @Override
    public Loader<List<Book>> onCreateLoader(int id, Bundle args) {
        showLoadingIndicator();
        return mApp.getmBookManager().getBookLoader();
    }

    @Override
    public void onLoadFinished(Loader<List<Book>> loader, List<Book> books) {
        if (loader instanceof OkAsyncTaskLoader) {
            Exception loadingException = ((OkAsyncTaskLoader) loader).loadingException;
            if (loadingException != null) {
                Log.e(TAG, "Get books failed");
                showError(loadingException);
                return;
            }
            showContent(books);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Book>> loader) {
        // not used
    }


    private void ensureGetBooksAsyncCallCancelled() {
        if (mGetBooksAsyncCall != null) {
            Log.d(TAG, "ensureGetBooksAsyncCallCancelled - cancelling the task");
            mGetBooksAsyncCall.cancel();
        }
    }


    public void showError(Exception e) {
        Log.e(TAG, "showError", e);
        new AlertDialog.Builder(getActivity())
                .setTitle("Error")
                .setMessage(e.getMessage())
                .setCancelable(true)
                .create()
                .show();
    }

    private void showLoadingIndicator() {
        Log.d(TAG, "showLoadingIndicator");
        mBookListView.setVisibility(View.GONE);
        mContentLoadingView.setVisibility(View.VISIBLE);
    }

    public void showContent(final List<Book> books) {
        Log.d(TAG, "showContent");
        mBookListAdapter = new BookListAdapter(this.getContext(), (ArrayList<Book>) books);
        mBookListView.setAdapter(mBookListAdapter);
        mContentLoadingView.setVisibility(View.GONE);
        mBookListView.setVisibility(View.VISIBLE);
    }


}