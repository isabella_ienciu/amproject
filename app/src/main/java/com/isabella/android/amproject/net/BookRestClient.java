package com.isabella.android.amproject.net;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.JsonReader;
import android.util.JsonWriter;
import android.util.Log;


import com.isabella.android.amproject.R;
import com.isabella.android.amproject.content.Book;
import com.isabella.android.amproject.content.LibraryDatabase;
import com.isabella.android.amproject.content.User;
import com.isabella.android.amproject.net.mapping.CredentialsWriter;
import com.isabella.android.amproject.net.mapping.TokenReader;
import com.isabella.android.amproject.util.BookWriter;
import com.isabella.android.amproject.util.Cancellable;
import com.isabella.android.amproject.util.OkCancellableCall;
import com.isabella.android.amproject.util.OnErrorListener;
import com.isabella.android.amproject.util.OnSuccessListener;
import com.isabella.android.amproject.util.ResourceListReader;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class BookRestClient {
    private static final String TAG = BookRestClient.class.getSimpleName();
    public static final String UTF_8 = "UTF-8";
    private static final String APPLICATION_JSON = "application/json";
    public static final String LAST_MODIFIED = "Last-Modified";

    private final OkHttpClient mOkHttpClient;
    private final String mApiUrl;
    private final String mBookUrl;
    private final String mAuthUrl;
    private User mUser;

    // temporary, to be removed later
    private List<Book> mBooks;

    public BookRestClient(Context context) {
        mOkHttpClient = new OkHttpClient();
        mApiUrl = context.getString(R.string.api_url);
        mBookUrl = mApiUrl.concat("/book");
        mAuthUrl = mApiUrl.concat("/auth");
        Log.d(TAG, "BookRestClient created");
        // temporary, to be removed later
        mBooks=new ArrayList<>();
        int n=10;
        for(int i=0;i<n;i++){
            Book b = new Book(Integer.toString(i),"title "+Integer.toString(i),"author ",new Date());
            mBooks.add(b);
        }
    }

    public List<Book> getAll() throws IOException { //sync operation
            return mBooks;

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void addAuthToken(Request.Builder requestBuilder) {
        User user;
        user=mUser;

        if (user != null) {
            requestBuilder.header("Authorization", String.format("Bearer %s", user.getToken()));
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public Cancellable searchAsync(
            String mNotesLastUpdate,
            final OnSuccessListener<LastModifiedList<Book>> successListener,
            final OnErrorListener errorListener) {
        Request.Builder requestBuilder = new Request.Builder().url(mBookUrl);
        if (mNotesLastUpdate != null) {
            requestBuilder.header(LAST_MODIFIED, mNotesLastUpdate);
        }
        addAuthToken(requestBuilder);
        return new CancellableOkHttpAsync<LastModifiedList<Book>>(
                requestBuilder.build(),
                new ResponseReader<LastModifiedList<Book>>() {
                    @Override
                    public LastModifiedList<Book> read(Response response) throws Exception {
                        JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), UTF_8));
                        if (response.code() == 304) { //not modified
                            return new LastModifiedList<Book>(response.header(LAST_MODIFIED), null);
                        } else {
                            return new LastModifiedList<Book>(
                                    response.header(LAST_MODIFIED),
                                    new ResourceListReader<Book>(new BookReader()).read(reader));
                        }
                    }
                },
                successListener,
                errorListener
        );
    }

    public Cancellable getToken(User user, OnSuccessListener<String> successListener, OnErrorListener errorListener) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JsonWriter writer = null;
        try {
            writer = new JsonWriter(new OutputStreamWriter(baos, UTF_8));
            new CredentialsWriter().write(user, writer);
            writer.close();
        } catch (Exception e) {
            Log.e(TAG, "getToken failed", e);
            throw new Exception(e);
        }
        return new CancellableOkHttpAsync<String>(
                new Request.Builder()
                        .url(String.format("%s/login", mAuthUrl))
                        .post(RequestBody.create(MediaType.parse(APPLICATION_JSON), baos.toByteArray()))
                        .build(),
                new ResponseReader<String>() {
                    @Override
                    public String read(Response response) throws Exception {
                        JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), UTF_8));
                        if (response.code() == 201) { //created
                            return new TokenReader().read(reader);
                        } else {
                            return null;
                        }
                    }
                },
                successListener,
                errorListener
        );
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public Cancellable addBook(String title, OnSuccessListener<String> successListener, OnErrorListener errorListener) throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JsonWriter writer = null;
        try {
            writer = new JsonWriter(new OutputStreamWriter(baos, UTF_8));
            new BookWriter().write(title, writer);
            writer.close();
        } catch (Exception e) {
            Log.e(TAG, "writeBook failed", e);
            throw new Exception(e);
        }
        Request.Builder requestBuilder = new Request.Builder().url(mBookUrl).post(RequestBody.create(MediaType.parse(APPLICATION_JSON), baos.toByteArray()));
        addAuthToken(requestBuilder);
        return new CancellableOkHttpAsync<String>(
                requestBuilder.build(),
                new ResponseReader<String>() {
                    @Override
                    public String read(Response response) throws Exception {
                        JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), UTF_8));
                        if (response.code() == 201) { //created
                            return "Book Added!";
                        } else {
                            return null;
                        }
                    }
                },
                successListener,
                errorListener
        );
    }

    public void setUser(User user) {
        this.mUser = user;
    }



    private static interface ResponseReader<E> {
        E read(Response response) throws Exception;
    }

    private class CancellableOkHttpAsync<E> implements Cancellable {
        private Call mCall;

        public CancellableOkHttpAsync(
                final Request request,
                final ResponseReader<E> responseReader,
                final OnSuccessListener<E> successListener,
                final OnErrorListener errorListener) {
            try {
                mCall = mOkHttpClient.newCall(request);
                Log.d(TAG, String.format("started %s %s", request.method(), request.url()));
                //retry 3x, renew token
                mCall.enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        notifyFailure(e, request, errorListener);
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        try {
                            notifySuccess(response, request, successListener, responseReader);
                        } catch (Exception e) {
                            notifyFailure(e, request, errorListener);
                        }
                    }
                });
            } catch (Exception e) {
                notifyFailure(e, request, errorListener);
            }
        }

        @Override
        public void cancel() {
            if (mCall != null) {
                mCall.cancel();
            }
        }

        private void notifySuccess(Response response, Request request,
                                   OnSuccessListener<E> successListener, ResponseReader<E> responseReader) throws Exception {
            if (mCall.isCanceled()) {
                Log.d(TAG, String.format("completed, but cancelled %s %s", request.method(), request.url()));
            } else {
                Log.d(TAG, String.format("completed %s %s", request.method(), request.url()));
                successListener.onSuccess(responseReader.read(response));
            }
        }

        private void notifyFailure(Exception e, Request request, OnErrorListener errorListener) {
            if (mCall.isCanceled()) {
                Log.d(TAG, String.format("failed, but cancelled %s %s", request.method(), request.url()));
            } else {
                Log.e(TAG, String.format("failed %s %s", request.method(), request.url()), e);
                errorListener.onError(e instanceof Exception ? e : new Exception(e));
            }
        }
    }

}
