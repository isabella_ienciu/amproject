package com.isabella.android.amproject;

import android.app.Application;
import android.util.Log;

import com.isabella.android.amproject.net.BookRestClient;
import com.isabella.android.amproject.service.BookManager;

/**
 * Created by Isabella on 10/19/2016.
 */

public class LibraryApplication extends Application {

    public static final String TAG = LibraryApplication.class.getSimpleName();
    private BookRestClient mBookRestClient;
    private BookManager mBookManager;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");
        mBookManager = new BookManager(this);
        mBookRestClient = new BookRestClient(this);
        mBookManager.setBookRestClient(mBookRestClient);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public BookManager getmBookManager() {
        return mBookManager;
    }
}

